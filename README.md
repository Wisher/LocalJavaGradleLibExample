An example of how to use local library jars in a gradle project

This can be done alongside maven and other hosted repository dependencies

To see how this is achieved, checkout the build.gradle file, necessary code is all there

the final builds will include a lib folder which has the library jars, executing the main jar will run the sample code which references the dependency jar

the javadocs however need to be read manually, so i included a folder with them and setup the .gitignore to ignore extracted javadocs
